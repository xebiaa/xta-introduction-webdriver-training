package nl.xebia.specs;

import nl.xebia.util.SharedDriver;
import org.testng.annotations.AfterClass;

public class BaseTest {

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        SharedDriver.closeBrowser();
    }

}
