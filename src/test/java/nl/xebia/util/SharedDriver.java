package nl.xebia.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SharedDriver {

    static private WebDriver instance;

    static public WebDriver getInstance() {
        if (instance == null) {
            instance = new FirefoxDriver();
        }
        return instance;
    }

    static public void closeBrowser() {
        instance.quit();
    }

}
