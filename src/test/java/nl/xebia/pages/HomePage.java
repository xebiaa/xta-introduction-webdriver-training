package nl.xebia.pages;

import org.testng.Assert;

public class HomePage extends BasePage {

    public HomePage() {
        super("http://www.bol.com/nl/index.html");
    }

    @Override
    protected void isLoaded() {
        Assert.assertEquals(driver.getTitle(), "bol.com | de winkel van ons allemaal | Welkom");
    }
}
